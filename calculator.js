
var operandA = null;
var operandB = null;
var result = 0;
var operator = null;


document.addEventListener('DOMContentLoaded', function() {
	var numEls = document.querySelectorAll('.number');
	console.log(numEls);
	for (var i = 0; i < numEls.length; i++) {
		console.log(numEls[i]);
		var myNum = numEls[i].id;
		var myHandler = number.bind(null, myNum);
		numEls[i].addEventListener('click', myHandler);
	}

	document.getElementById("+").addEventListener('click', add);
	document.getElementById("-").addEventListener('click', minus);
	document.getElementById("X").addEventListener('click', multiply);
	document.getElementById("/").addEventListener('click', divide);
	document.getElementById("=").addEventListener('click', calculate);
	document.getElementById("Clear").addEventListener('click', clear);
});






function number(x) {
	if (operator==null) {
		if (operandA == null) operandA = x;
		else operandA += x; //concatenate as string
		document.getElementById("result").innerHTML = operandA.toString();
	} else {
		if (operandB == null) operandB = x;
		else operandB += x;
		document.getElementById("result").innerHTML = operandB.toString();
	}
}


function add() {
	
	operator = "add";
	
}

function minus() {
	operator = "minus";
}

function multiply() {
	operator = "multiply";
}

function divide() {
	operator = "divide";
}

function calculate() {
	if(operator ==="add"){
		if (operandA != null) result = (operandA-0)+(operandB-0);
		else result = (result-0) + (operandB-0);
	}
	else if(operator ==="minus"){
		if (operandA != null) result = operandA-operandB;
		else result -= operandB;
	}
	else if(operator ==="multiply"){
		if (operandA != null) result = operandA*operandB;
		else result = result * operandB;
	}
	else{
		if (operandA != null) result = operandA/operandB;
		else result = result/operandB;
	}

	document.getElementById("result").innerHTML = result.toString();

	operandA = null;
	operandB = null;
	operator = null;
		
}
function clear() {
		operandA = null;
		operandB = null;
		operator = null;
		result = 0;
		document.getElementById("result").innerHTML = result.toString();
	}